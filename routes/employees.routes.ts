import { Router } from 'express';
import EmployeeController from '../controllers/employees.controller';

const router = Router();

router.get('/employees', EmployeeController.getEmployees);

export default router;
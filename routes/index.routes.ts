import EmployeeRoutes from './employees.routes'
import CustomerRoutes from './customers.routes'

export function addRoutes(app:any){
    app.use(EmployeeRoutes);
    app.use(CustomerRoutes);
}
import { Request, Response } from 'express';
import { customer } from '../data/customer'


export class Customer {

    async getCustomers (req: Request, res: Response): Promise<any> {
        return res.status(200).json(customer);
    }
    
}

const customerClass = new Customer();
export default customerClass;
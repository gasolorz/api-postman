import { Request, Response } from 'express';
import { employees } from '../data/employee'


export class Employee {

    async getEmployees (req: Request, res: Response): Promise<any> {
        return res.status(200).json(employees);
    }
    
}

const employee = new Employee();
export default employee;
import express from 'express';
import { addRoutes } from './routes/index.routes';

const app = express();
const PORT = 8000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));

addRoutes(app);

app.listen(process.env.PORT || PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});